<?php


namespace Shires82\DesignPatterns\Visitor\Tests;

use PHPUnit\Framework\TestCase;
use Shires82\DesignPatterns\Visitor\TaxVisitor;
use Shires82\DesignPatterns\Visitor\TobaccoProduct;


class VisitorTest extends TestCase
{

    public function testTaxHasBeenAddedToProductPrice()
    {
        $price = 5.99;

        $cigars = new TobaccoProduct($price);
        $this->assertEquals($price, $cigars->getPrice());

        $cigars->accept(new TaxVisitor());
        $expectedGross = 7.19;
        $this->assertEquals($expectedGross, $cigars->getGross());
    }

}
