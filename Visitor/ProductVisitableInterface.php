<?php


namespace Shires82\DesignPatterns\Visitor;


interface ProductVisitableInterface
{
    public function accept(ProductVisitorInterface $visitor);
}