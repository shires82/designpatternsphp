<?php


namespace Shires82\DesignPatterns\Visitor;


class TobaccoProduct implements ProductInterface, ProductVisitableInterface
{
    /**
     * @var float
     */
    protected $price;

    /**
     * @var float
     */
    protected $gross;

    /**
     * @var bool
     */
    protected $isTaxable;


    public function __construct(float $price)
    {
        $this->price = $price;
        $this->isTaxable = true;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function getGross(): float
    {
        return $this->gross;
    }

    /**
     * @param float $gross
     */
    public function setGross(float $gross)
    {
        $this->gross = $gross;
    }

    /**
     * @return bool
     */
    public function isTaxable(): bool
    {
        return $this->isTaxable;
    }

    /**
     * @param ProductVisitorInterface $visitor
     */
    public function accept(ProductVisitorInterface $visitor)
    {
        $visitor->visit($this);
    }

}
