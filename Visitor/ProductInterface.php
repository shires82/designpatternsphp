<?php


namespace Shires82\DesignPatterns\Visitor;


interface ProductInterface
{
    public function getGross(): float;

    public function getPrice(): float;

    public function setGross(float $gross);

    public function isTaxable(): bool;
}