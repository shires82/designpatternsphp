<?php


namespace Shires82\DesignPatterns\Visitor;


interface ProductVisitorInterface
{
    public function visit(ProductInterface $product);
}