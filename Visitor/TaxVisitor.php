<?php


namespace Shires82\DesignPatterns\Visitor;


class TaxVisitor implements ProductVisitorInterface
{
    const TAX_RATE = 1.2;

    public function visit(ProductInterface $product)
    {
        if ($product->isTaxable()) {
            $gross = round($product->getPrice() * self::TAX_RATE, 2);
            $product->setGross($gross);
        }
    }

}