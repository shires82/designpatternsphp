#!/usr/bin/env bash

# Install git / php 7.1
add-apt-repository ppa:ondrej/php && \
apt-get update && \
apt-get install -y git php7.1 php7.1-common php7.1-dom php7.1-mbstring php7.1-xml php7.1-soap php7.1-xdebug && \

# Install composer
cd /vagrant && \
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"

OUTPUT="$(php -r "if (hash_file('SHA384', 'composer-setup.php') == trim(file_get_contents('https://composer.github.io/installer.sig'))) { echo 'Y'; } else { unlink('composer-setup.php'); }")"
if [ $OUTPUT == "Y" ]; then
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer && \
    rm composer-setup.php && \
    cd /vagrant && \
    composer install
fi
