<?php
/**
 * Authored by: Neil Johnson
 * 04/05/2017
 */

namespace Shires82\DesignPatterns\Factory;


class FerrariFactory implements CarFactoryInterface
{
    /**
     * @var string
     */
    protected $colour;

    /**
     * @var string
     */
    protected $model;

    /**
     * FerrariFactory constructor.
     * @param string $colour
     * @param string $model
     */
    public function __construct(string $colour, string $model)
    {
        $this->colour = $colour;
        $this->model = $model;
    }

    /**
     * {@inheritdoc}
     */
    public function build(): Car
    {
        $car = (new SportsCar())
            ->setManufacturer('Ferrari')
            ->setColour($this->colour)
            ->setModel($this->model);

        return $car;
    }

}