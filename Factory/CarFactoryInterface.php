<?php
/**
 * Authored by: Neil Johnson
 * 04/05/2017
 */

namespace Shires82\DesignPatterns\Factory;


interface CarFactoryInterface
{
    /**
     * Builds a car object.
     *
     * @return Car
     */
    public function build(): Car;

}
