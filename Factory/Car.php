<?php
/**
 * Authored by: Neil Johnson
 * 04/05/2017
 */

namespace Shires82\DesignPatterns\Factory;


abstract class Car
{
    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $manufacturer;

    /**
     * @var string
     */
    protected $model;

    /**
     * @var string
     */
    protected $colour;

    /**
     * @param string $type
     * @return Car
     */
    protected function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param string $manufacturer
     * @return Car
     */
    public function setManufacturer(string $manufacturer): self
    {
        $this->manufacturer = $manufacturer;
        return $this;
    }

    /**
     * @param string $model
     * @return Car
     */
    public function setModel(string $model): self
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @param string $colour
     * @return Car
     */
    public function setColour(string $colour): self
    {
        $this->colour = $colour;
        return $this;
    }
}
