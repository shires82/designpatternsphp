<?php

namespace Shires82\DesignPatterns\Factory\Tests;


use PHPUnit\Framework\TestCase;
use Shires82\DesignPatterns\Factory\FerrariFactory;
use Shires82\DesignPatterns\Factory\SportsCar;

class FactoryTest extends TestCase
{
    public function testCarFactoryCanCreateASportsCar()
    {
        $expected = (new SportsCar())
            ->setColour('red')
            ->setManufacturer('Ferrari')
            ->setModel('430');

        $factory = new FerrariFactory('red', '430');

        $this->assertEquals($expected, $factory->build());
    }
}
