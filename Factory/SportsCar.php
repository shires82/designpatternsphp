<?php
/**
 * Authored by: Neil Johnson
 * 04/05/2017
 */

namespace Shires82\DesignPatterns\Factory;


class SportsCar extends Car
{
    public function __construct()
    {
        $this->setType('Sports');
    }
}